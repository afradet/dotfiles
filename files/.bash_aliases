# Linux cmd
alias lla="ls -la"
alias lrt="ls -lrt"
alias xclip="xclip -selection c"
alias untar="tar -zxvf "
alias c='clear'
alias ipe='curl ipinfo.io/ip'
# alias ipi='ipconfig getifaddr en0'

# fs navigation :
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

# Folders
alias wksp="cd ~/workspace/"
alias sudoap="sudo apt update && sudo apt upgrade -y"
alias work="wksp && cd work"
alias ops="wksp && cd ops"
alias perso="wksp && cd perso"
alias conf="wksp && cd conf"
alias gtd="wksp && cd perso/gtd"

# Dev
alias k=kubectl
alias tf=terraform

# Utils
alias ports='netstat -tulanp'
## shortcut  for iptables and pass it via sudo#
alias ipt='sudo /sbin/iptables'
# display all rules #
alias iptlist='sudo /sbin/iptables -L -n -v --line-numbers'
alias iptlistin='sudo /sbin/iptables -L INPUT -n -v --line-numbers'
alias iptlistout='sudo /sbin/iptables -L OUTPUT -n -v --line-numbers'
alias iptlistfw='sudo /sbin/iptables -L FORWARD -n -v --line-numbers'
alias firewall=iptlist
