#!/bin/bash

wget "https://dlcdn.apache.org/maven/maven-3/3.9.4/binaries/apache-maven-3.9.4-bin.tar.gz" -O maven.tar.gz && \
	sudo tar xf maven.tar.gz -C /opt && \
	sudo ln -s /opt/apache-maven-3.9.4 /opt/maven && \
	rm maven.tar.gz
