#!/bin/bash

if ! command -v node &> /dev/null
then
    echo "node could not be found"
    exit
fi

sudo apt -y install curl gpg unzip

echo '==> Installing aws cli'
gpg --import ressources/aws-gpg-key
cd /tmp
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip.sig" -o "awscliv2.sig"

# TODO automatic check
gpg --verify awscliv2.sig awscliv2.zip
read -p 'Is gpg OK ? [y/n] ' continue_install
if [[ $continue_install = 'n' ]]
then 
	echo 'Cancelling installation'
	exit
fi

unzip awscliv2.zip
sudo ./aws/install
aws --version

echo '==> Installing aws SAM'
curl -L "https://github.com/aws/aws-sam-cli/releases/latest/download/aws-sam-cli-linux-x86_64.zip" -o "aws-sam-cli-linux-x86_64.zip"
unzip aws-sam-cli-linux-x86_64.zip -d sam-installation
sudo ./sam-installation/install
sam --version

echo '==> Installing aws CDK'
sudo npm install -g aws-cdk@latest

echo '==> Cleaning up'
cd /tmp # Just in case i forgot and move to other dir before
rm -rf aws aws-sam-cli-* awscliv2 sam-installation
