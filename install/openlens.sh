#!/bin/bash

curl -s https://api.github.com/repos/MuhammedKalkan/OpenLens/releases/latest \
| grep "browser_download_url.*amd64\.deb\"" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget --output-document=openlens.deb -qi - \
&& sudo dpkg -i openlens.deb \
&& rm openlens.deb