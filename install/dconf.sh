#!/bin/bash

# Config
gsettings set org.gnome.desktop.calendar show-weekdate true
gsettings set org.gnome.desktop.interface gtk-theme 'Yaru-dark'
gsettings set org.gnome.shell.extensions.dash-to-dock dock-position 'BOTTOM'
gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed false

# Shortcuts
gsettings set org.gnome.settings-daemon.plugins.media-keys home "['<Super>e']"
