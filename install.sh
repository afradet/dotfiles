#!/bin/bash

sudo apt update && sudo apt upgrade -y


# Basic packages
sudo apt install -y \
    software-properties-common \
    ca-certificates \
    apt-transport-https \
    apt-utils

# Management utils
sudo apt install -y \
    htop \
    net-tools

# Desktop utils
# - meld : diff and merge files
# - ffmpeg : video thumbnail in nautilus
sudo apt install -y \
    curl \
    wget \
    gnupg \
    gnome-tweaks \
    guake \
    xclip \
    meld \
    ffmpeg \
    ffmpegthumbnailer \
    xournal \
    imagemagick \
    ghostscript \
    gimp \
    chromium-browser \
    calibre \
    keepassxc \
    vlc \
    transmission \
    libreoffice \
    snapd \
    nano \
    vim

# Dev utils
sudo apt install -y \
  python3-pip \
  shellcheck \
  git \
  apache2 \
  jq \
  nginx \
  ufw \
  postgresql-client \
  postgresql-client-common \
  libpq-dev

# App image launcher
#sudo add-apt-repository ppa:appimagelauncher-team/stable
#sudo apt update && sudo apt -y install appimagelauncher

# pcloud requirement
sudo add-apt-repository universe && sudo apt install libfuse2


for file in install/*.sh; do
	bash $file
done

# Snaps
snap install chromium discord firefox signal-desktop btop


# Vivaldi TODO update version if needed
cd /tmp && wget "https://downloads.vivaldi.com/stable/vivaldi-stable_6.1.3035.302-1_amd64.deb" --output-document=vivaldi.deb && sudo dpkg -i vivaldi.deb && cd -

# Firefox develop edition
cd /tmp && wget "https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=linux64&lang=fr" --output-document=firefox.tar.gz && \
    tar xjf firefox.tar.gz && \
    sudo cp -rp firefox /opt/firefox-dev && \
    sudo chown -R $USER /opt/firefox-dev && \
    cd - && \
    cp files/firefox_dev.desktop ~/.local/share/applications/

# gws
wget https://raw.githubusercontent.com/StreakyCobra/gws/master/src/gws -O gws && mv gws ~/.local/bin/ && chmod +x ~/.local/bin/gws

# TODO add copy of git commit template
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Bash
ln -sf $SCRIPT_DIR/files/.bash_aliases ~/.bash_aliases
ln -sf $SCRIPT_DIR/files/.bash_commons ~/.bash_commons
# Git
ln -sf $SCRIPT_DIR/files/.gitconfig ~/.gitconfig
ln -sf $SCRIPT_DIR/files/gitCommitTemplate.txt ~/gitCommitTemplate.txt
